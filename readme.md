
*Please do note, this guide uses https://zeronet.link as our dns replacement due to the sites original functionality lining up with what we need*

for zeronet I was required to alter how the container started to use a `start.sh` file rather than a direct command, because it litterally was not accepting the ui_host argument through the commandline directly.

### Required Steps
1) you need to have docker installed on the machine you are going to be using zeronet from
2) you need to have a self signed SSL RootCA installed on your system, see your operating systems instructions for this
	* this system is setup for [[github] minica](https://github.com/jsha/minica), please use it 
3) you will need to beable to change your DNS settings system wide for this to work, otherwise you will have to setup your DNS each time you connect
	* For windows, I suggest downloading and installing [[clearnet] Nirsoft's QuickSetDNS](https://www.nirsoft.net/utils/quick_set_dns.html) 

### How to use this
1) Do all the required steps
2) Create an Localhost domain entry
	1. (optional) create an SSL cert for localhost
	2. put the cert and key inside `caddy/localhost/`
	3. create `caddy.conf` inside `caddy/localhost/` with the following
	```
	localhost {
		tls localhost/cert.pem localhost/key.pem
		reverse_proxy pihole
	}
	```
3) head to `localhost` and configure pi-hole as you would, 
	* inside of "local dns" settings, add an entry for `zeronet.link` that points to `127.0.0.1`, 
	* this should be default with this, but please do double check
4) Create a `zeronet.link` domain entry in `caddy/zeronet.link`, 
	```
	zeronet.link {
		tls /config/zeronet.link/cert.pem /config/zeronet.link/key.pem
		reverse_proxy zeronet:43110
	}
	```

Note, the file structure required for a ssl certified domains locally is as follows
```
docker-compose-root/
	caddy/
		zeronet.link/
			/caddy.conf
			/cert.pem
			/key.pem
```