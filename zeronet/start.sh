#!/bin/sh
if [ $ENABLE_TOR != "" ]; then
	tor&
fi

python3 zeronet.py --ui_ip 0.0.0.0 --ui_host "zeronet.link" --fileserver_port 26552