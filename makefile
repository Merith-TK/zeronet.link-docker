default: fix-origin
	git push gitlab
	git push gitcenter

fix-origin:
	-git remote rm origin
	-git remote rm gitlab
	-git remote rm gitcenter
	git remote add gitlab git@gitlab.com:Merith-TK/zeronet.link-docker.git
	git remote add gitcenter D:/.docker/zeronet/14hAxMRDjoHwu2SsaJWWLkqsin2qPzw38d/zeronet-link-docker.git