you need to use minica with this setup,

This setup is meant for minica, so you need to "replicate" its setup per config

how to use this setup, 
1) install minica
2) enter this folder with a shell or command prompt
3) run `minica -domains localhost`
4) take the `minica.pem` and `minica-key.pem` files and import them as a Trusted Root CA on your computer.
5) Inside the newly made "localhost" folder, create a file called "`caddy.conf`" and populate it with your caddy config,
6) enjoy

note: if you have your own RootCA keys you wihs to use, replace the "minica.pem" and "minica-key.pem" files with yours respectively